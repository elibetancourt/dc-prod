({
	doInit: function(component, event, helper) {
		var action;
		action = component.get("c.getPassportPictureUrl");
		action.setParams({ 
			recordId: component.get("v.recordId")
		});
		action.setCallback(this, function(response) {
			switch (response.getState()) {
				case "SUCCESS":
					component.set("v.currentUrl", response.getReturnValue());
					break;
				case "ERROR":
					component.set("v.currentUrl", null);
					break;
			}
		});
		$A.enqueueAction(action);
	},
	handleUploadFinished: function(component, event, helper) {
		// Get the list of uploaded files
		component.set("v.stage", "saving");
        event.getParam("files").forEach(function(file) {
        	var action;
			action = component.get("c.savePassportPictureUrl");
			action.setParams({ 
				recordId: component.get("v.recordId"),
				documentId: file.documentId
			});
			action.setCallback(this, function(response) {
				switch (response.getState()) {
					case "SUCCESS":
						console.log(response.getReturnValue());
						component.set("v.currentUrl", response.getReturnValue());
						component.set("v.stage", "idle");
						break;
					case "ERROR":
						component.set("v.stage", "error");
						break;
				}
			});
			$A.enqueueAction(action);
        });
	}
})