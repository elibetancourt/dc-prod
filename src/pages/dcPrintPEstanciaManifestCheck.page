<!--
  @File Name          : dcPrintPEstanciaManifestCheck.page
  @Description        : 
  @Author             : Silvia Velazquez
  @Group              : 
  @Last Modified By   : Silvia Velazquez
  @Last Modified On   : 08-28-2020
  @Modification Log   : 
  Ver       Date            Author      		    Modification
  1.0    14/8/2020   Elizabeth Betancourt Herrera     Initial Version
-->
<apex:page showHeader="false" sidebar="false" standardStylesheets="false" standardController="Manifest__c" renderAs="pdf" applyBodyTag="false">
	<head>
        <style>
            
            @page {
                @top-center { 
            		content: element(header); 
            	}
            
	            @bottom-center {
                    content: element(footer);
                }
            
   				margin-top: 43px;
            }
            
            div.header {
	            width: 715px;
	            font-family: Arial Unicode MS;
                position: running(header);
            	margin-top: 30px;
            }

            div.header2 {
	            width: 715px;
	            font-family: Arial Unicode MS;
            	margin-top: 35px;
            }

            div.header3 {
	            width: 715px;
	            font-family: Arial Unicode MS;
            	margin-top: 7px;
                page-break-before: always;
            }
            
            div.footer {
	            font-family: Arial Unicode MS;
            	font-size: 12px;
            	text-align: center;
            	padding: 5px;
                position: running(footer);
            }

            div.subfooter {
            	display: inline-block;
            }
            
            .pagenumber:before {
                content: counter(page);
            }

            .pagecount:before {
            	content: counter(pages);
            }
            
            .content{
            	font-family: Arial Unicode MS;
            	margin-top: 30px;
            }

            .tdBold {
                font-weight: bold !important;
            }

            .checkSpace{
                padding-left: 2px; 
                padding-top: 90px
            }

            .rowHeight{
                height: 25px;
            }

            .manifName{
                width: 600px;
                margin-top: 58px;
            }

            .dollar{
                text-align: right; 
                padding-left: 100px;
            }
        </style>
    </head>
    <apex:variable var="count" value="{!1}"/>
    <apex:repeat value="{!Manifest__c.Procedures__r}" var="item">
        <!-- Print first Check -->
        <!--<div style="page-break-after:always;">   PAGE BREAK-->
            <div class="{!IF((count != 1),'header3','header2')}">
                <table style="width: 100%; font-size: 12px;">
                    <tr style="height: 62px">
                        <td style="text-align: left; width: 50%; vertical-align: top;">             
                            <!-- <apex:image value="https://districtcuba-dev-ed--c.documentforce.com/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Jpeg&versionId=0683h0000025JD3&operationContext=CHATTER&contentId=05T3h00000D50ka" height="80px" width="80px"/> -->
                        </td>
                        <td style="text-align: right; padding-right: 9px">
                            <apex:outputText value="{0,date,MM/dd/yyyy}">
                                <apex:param value="{! NOW() }" />
                            </apex:outputText>       
                        </td>
                    </tr> 
                    <tr style="padding-top: 30px height: 60px">
                        <td style="text-align: left; padding-left: 72px"> 
                            EMBAJADA DE LA REPUBLICA DE CUBA
                        </td>
                        <td style="text-align: right; padding-right: 67px; font-weight: 900">
                            <apex:outputText value="{0, number, ###,###,###,##0.00}">
                                <apex:param value="{!item.ProcedureFirstCheckAmount__c}"/>
                            </apex:outputText>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: left; padding-top: 25px; padding-left: 10px">
                            <apex:outputText value="{! IF((item.ProcedureFirstCheckAmountInWords__c == null), 'NO DEFINIDO', item.ProcedureFirstCheckAmountInWords__c) & ' ***********'}"/>
                            <!-- AQUI VA LA DESCRIPCION EN PALABRAS DEL 1er MONTO TOTAL*********** -->
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table class = "manifName">
                                <tr>
                                    <td style="font-size: 10px; padding-left: 32px">
                                        Manifiesto #
                                        <apex:outputField value="{!Manifest__c.Name}"/>
                                        - Factura #
                                        <!--<apex:outputText value="{!count}"/> -->
                                        <apex:outputText value="{!item.Name}"/>
                                        - &nbsp;
                                        <apex:outputText value="{!UPPER(item.Customer__r.FirstName) & ' ' & UPPER(item.Customer__r.Middle_Name__c) 
                                                    & ' ' & UPPER(item.Customer__r.LastName) & ' ' & UPPER(item.Customer__r.Second_Last_Name__c)}" 
                                                rendered="{!NOT(ISNULL(item.Customer__r.Middle_Name__c))}"/>

                                        <apex:outputText value="{!UPPER(item.Customer__r.FirstName) & ' ' & UPPER(item.Customer__r.LastName) & ' ' & 
                                                        UPPER(item.Customer__r.Second_Last_Name__c)}" 
                                                rendered="{!ISNULL(item.Customer__r.Middle_Name__c)}"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!--Check Details Copy 1, First Check-->
                    <tr>
                        <td colspan="2" class="checkSpace">
                            <table style="width: 100%; margin-top: 40px"> <!--margin-top: 60px-->
                                <tr class="rowHeight">
                                    <td style="width:80px">
                                        <b>Factura #:</b>
                                    </td>
                                    <td style="text-align:left;">
                                        <!--<apex:outputText value="{!count}"/> -->
                                        <apex:outputText value="{!item.Name}"/> <!-- No. de tramite para ver el orden de impresion de los tramites -->
                                        &nbsp;
                                        &nbsp;
                                        &nbsp;
                                        &nbsp;
                                        <b>Check #:</b> &nbsp;
                                        <apex:outputText value="{!item.ProcedureFirstCheckNumber__c}"/>
                                    </td>
                                    <!-- <td style="text-align:left; padding-left: 20px">
                                        <b>Check #:</b> &nbsp;
                                        <apex:outputText value="{!item.ProcedureFirstCheckNumber__c}"/>
                                    </td> -->
                                    <td class="dollar"> <!--padding-left: 100px-->
                                        $
                                    </td>
                                    <td style="text-align: right; padding-right: 67px; font-weight: 900">
                                        <apex:outputText value="{0, number, ###,###,###,##0.00}">
                                            <apex:param value="{!item.ProcedureFirstCheckAmount__c}"/>
                                        </apex:outputText>
                                    </td>
                                </tr>
                                <tr class="rowHeight">
                                    <td colspan="4">
                                        <b>Manifiesto #: </b> &nbsp;
                                        <apex:outputField value="{!Manifest__c.Name}"/> 
                                    </td>
                                </tr>
                                <tr class="rowHeight">
                                    <td colspan="4">
                                        <b>Nombre:</b> &nbsp;
                                        <apex:outputText value="{!IF(ISNULL(item.Customer__r.Middle_Name__c), UPPER(item.Customer__r.FirstName) & ' ' & 
                                                                    UPPER(item.Customer__r.LastName) & ' ' & UPPER(item.Customer__r.Second_Last_Name__c), 
                                                                    UPPER(item.Customer__r.FirstName) & ' ' & UPPER(item.Customer__r.Middle_Name__c) 
                                                                    & ' ' & UPPER(item.Customer__r.LastName) & ' ' & UPPER(item.Customer__r.Second_Last_Name__c))}"/>
                                    </td>
                                </tr>
                                <tr class="rowHeight">
                                    <td colspan="4">
                                        <b>Categoria:</b> &nbsp;
                                        <apex:outputText value="{!UPPER(item.RecordType.Name)}"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!--Check Details Copy 2, First Check-->
                    <tr>
                        <td colspan="2" class="checkSpace">
                            <table style="width: 100%; margin-top: 70px"> <!--margin-top: 60px-->
                                <tr class="rowHeight">
                                    <td style="width:80px">
                                        <b>Factura #:</b>
                                    </td>
                                    <td style="text-align:left;">
                                        <!--<apex:outputText value="{!count}"/> -->
                                        <apex:outputText value="{!item.Name}"/> <!-- No. de tramite para ver el orden de impresion de los tramites -->
                                        &nbsp;
                                        &nbsp;
                                        &nbsp;
                                        &nbsp;
                                        <b>Check #:</b> &nbsp;
                                        <apex:outputText value="{!item.ProcedureFirstCheckNumber__c}"/>
                                    </td>
                                    <!-- <td style="text-align:left; padding-left: 20px">
                                        <b>Check #:</b> &nbsp;
                                        <apex:outputText value="{!item.ProcedureFirstCheckNumber__c}"/>
                                    </td> -->
                                    <td class="dollar"> <!--padding-left: 100px-->
                                        $
                                    </td>
                                    <td style="text-align: right; padding-right: 67px; font-weight: 900">
                                        <apex:outputText value="{0, number, ###,###,###,##0.00}">
                                            <apex:param value="{!item.ProcedureFirstCheckAmount__c}"/>
                                        </apex:outputText>
                                    </td>
                                </tr>
                                <tr class="rowHeight">
                                    <td colspan="4">
                                        <b>Manifiesto #: </b> &nbsp;
                                        <apex:outputField value="{!Manifest__c.Name}"/> 
                                    </td>
                                </tr>
                                 <tr class="rowHeight">
                                    <td colspan="4">
                                        <b>Nombre:</b> &nbsp;
                                        <apex:outputText value="{!IF(ISNULL(item.Customer__r.Middle_Name__c), UPPER(item.Customer__r.FirstName) & ' ' & 
                                                                    UPPER(item.Customer__r.LastName) & ' ' & UPPER(item.Customer__r.Second_Last_Name__c), 
                                                                    UPPER(item.Customer__r.FirstName) & ' ' & UPPER(item.Customer__r.Middle_Name__c) 
                                                                    & ' ' & UPPER(item.Customer__r.LastName) & ' ' & UPPER(item.Customer__r.Second_Last_Name__c))}"/>
                                    </td>
                                </tr>
                                <tr class="rowHeight">
                                    <td colspan="4">
                                        <b>Categoria:</b> &nbsp;
                                        <apex:outputText value="{!UPPER(item.RecordType.Name)}"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>                  
                </table>
            </div>
        <apex:variable var="count" value="{!count + 1}"/>
    </apex:repeat>                        
</apex:page>