/**
 * @description       : 
 * @author            : Silvia Velazquez
 * @group             : 
 * @last modified on  : 08-31-2020
 * @last modified by  : Silvia Velazquez
 * Modifications Log 
 * Ver   Date         Author                   Modification
 * 1.0   07-22-2020   William Santana Méndez   Initial Version
**/
@isTest
public class SP_GeneraPasaporteProrrogaXMLTest {
    @TestSetup
    static void makeData(){
        
        Account account1=SP_XMLDataFactory.createAccount();
        insert account1;
        
        Contact contact1=SP_XMLDataFactory.createContact();
        insert contact1;        

        List<Manifest__c> manifiestoList=SP_XMLDataFactory.createManifestList();
        insert manifiestoList;

        List<Procedure__c> procedureListFull=SP_XMLDataFactory.createProcedureListFull(contact1, account1, manifiestoList);
        insert procedureListFull;              
    }

    @isTest
    public static void checkXMLProrroga(){
        Test.startTest();
        List<Manifest__c> manifestList = [SELECT Id, Name FROM Manifest__c WHERE Type__c = 'Prorroga'];
        List<String> manifiestos = new List<String>();
        manifiestos.add(manifestList[0].Id);
        GeneraPasaporteProrrogaXML.generaXML(manifiestos);
        
        ContentDocumentLink zipFile = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =:manifiestos.get(0) LIMIT 1];
        Boolean exists=false;
        if(zipFile!=null){
            exists=true;   
        }     
        System.assertEquals(true,exists,'');
        Test.stopTest();
    }
}