/**
 * @File Name          : dcFedExServiceInvocable.cls
 * @Description        : Class with invocable method to generate a shipment
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Silvia Velazquez
 * @Last Modified On   : 4/8/2020 5:03:14 p. m.
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    30/6/2020   Silvia Velazquez     Initial Version
**/
public with sharing class dcFedExServiceInvocable {

    @InvocableMethod(label='Generate Tracking' description='Generate Shipment Information and Label file.')
    public static List<String> generateTracking(List<InputParam> params) {
        dcShipmentManager mgr = new dcShipmentManager();
        String tipo = String.isNotBlank(params[0].tipoTramite) ? params[0].tipoTramite : '';
        String result = mgr.generateShipment(true,params[0].tramiteId,params[0].configurationId,null, null, null,null,null,null,null,null,tipo);
        List<String> trackingsID = new List<String>{result};
          return trackingsID;
    }

    //input Ids that comes to apex from flow
    public class InputParam{
    
        @InvocableVariable(required=true)
        public Id tramiteId;
        
        @InvocableVariable(required=true)
        public Id configurationId;

        @InvocableVariable
        public String tipoTramite;        
    }    
}