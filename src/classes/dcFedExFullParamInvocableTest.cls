/**
 * @File Name          : dcFedExFullParamInvocableTest.cls
 * @Description        : Test Class for dcFedExFullParamInvocable
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Ibrahim Napoles
 * @Last Modified On   : 08-26-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    22/7/2020   Silvia Velazquez     Initial Version
**/
@isTest
private class dcFedExFullParamInvocableTest {

    @isTest
	static void generateTestTrackingFullParamOK(){
        
        dcFedExFullParamInvocable.InputParam inputParam = new dcFedExFullParamInvocable.InputParam();
        inputParam.loteId = TestDataFactory.getLoteParam(true);
        inputParam.serviceType = 'FedEx 2Day AM';
        inputParam.packageType = 'FedEx Envelope';
        inputParam.packageCount = 1;
        inputParam.weight = 1;
        inputParam.height = 5;
        inputParam.length = 5;
        inputParam.width = 5;
        List<dcFedExFullParamInvocable.InputParam> listParams = new List<dcFedExFullParamInvocable.InputParam>{inputParam};
        List<String> result = new List<String>();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new dcFedexServiceCalloutMock('SUCCESS')); //generating a success response
        result = dcFedExFullParamInvocable.generateTrackingFullParam(listParams);
        Test.stopTest();
        System.assertEquals(true, String.isNotBlank(result[0]), '');
    }

    @isTest
	static void generateTestTrackingFullInvalidId(){
        Test.startTest();
        dcFedExFullParamInvocable.InputParam inputParam = new dcFedExFullParamInvocable.InputParam();
        inputParam.loteId = TestDataFactory.getLoteParam(false);
        inputParam.serviceType = 'FedEx 2Day AM';
        inputParam.packageType = 'FedEx Envelope';
        inputParam.packageCount = 1;
        inputParam.weight = 1;
        List<dcFedExFullParamInvocable.InputParam> listParams = new List<dcFedExFullParamInvocable.InputParam>{inputParam};
        List<String> result = new List<String>();

        Test.setMock(WebServiceMock.class, new dcFedexServiceCalloutMock('ERROR')); //generating an error responses
        result = dcFedExFullParamInvocable.generateTrackingFullParam(listParams);
        Test.stopTest();
        System.assertEquals(null, result[0], '');
    }
    
    @isTest
	static void generateTestTrackingFullInvalidPack(){
        Test.startTest();
        dcFedExFullParamInvocable.InputParam inputParam = new dcFedExFullParamInvocable.InputParam();
        inputParam.loteId = TestDataFactory.getLoteParam(true);
        inputParam.serviceType = 'FedEx 2Day AM';
        inputParam.packageType = 'FedEx-Envelop';
        inputParam.packageCount = 1;
        inputParam.weight = 1;
        List<dcFedExFullParamInvocable.InputParam> listParams = new List<dcFedExFullParamInvocable.InputParam>{inputParam};
        List<String> result = new List<String>();
        
        Test.setMock(WebServiceMock.class, new dcFedexServiceCalloutMock('ERROR')); //generating an error responses
        result = dcFedExFullParamInvocable.generateTrackingFullParam(listParams);
        Test.stopTest();
        System.assertEquals(null, result[0], '');
    }    

}