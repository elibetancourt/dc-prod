/**
 * @File Name          : dcApplyPaymentUtility.cls
 * @Description        :
 * @Author             : Silvia Velazquez
 * @Group              :
 * @Last Modified By   : Elizabeth Betancourt Herrera
 * @Last Modified On   : 10-15-2020
 * @Modification Log   :
 * Ver       Date            Author                 Modification
 * 1.0    20/8/2020   Silvia Velazquez     Initial Version
 * 1.1   08-24-2020   Ibrahim Napoles          Validate CRUD permission before SOQL/DML operation.
 **/
public with sharing class dcApplyPaymentUtility {

    /**
     * @description Retrieves a Batch records info by a given Id.
     * @author Silvia Velazquez | 20/8/2020
     * @param batchId
     * @return dcApplyPaymentController.Batch_Wrapper
     **/
    public static dcApplyPaymentController.Batch_Wrapper getBatchData(String batchId){
        if(!(Schema.sObjectType.Batch__c.isAccessible() &&
             Schema.sObjectType.Batch__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Batch__c.fields.Name.isAccessible() &&
             Schema.sObjectType.Batch__c.fields.Amount_Paid__c.isAccessible() &&
             Schema.sObjectType.Batch__c.fields.Amount_Due__c.isAccessible() &&
             Schema.sObjectType.Batch__c.fields.Monto_Aplicado__c.isAccessible() &&
             Schema.sObjectType.Batch__c.fields.Amount_Pending_to_Apply__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Name.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Batch__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Procedure__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Payment_Date__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Applied_Cash__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Contact.isAccessible() &&
             Schema.sObjectType.Contact.fields.Name.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.RecordTypeId.isAccessible() &&
             Schema.sObjectType.RecordType.isAccessible() &&
             Schema.sObjectType.RecordType.fields.Name.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Customer__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.CreatedDate.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Consulate_Payment__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Agency_Commission__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Wholesaler_Payment__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Name.isAccessible())) {
            DCException.throwPermiException('dcApplyPaymentUtility.getBatchData');
        }
        
        List<Batch__c> batchs = [SELECT Name, Amount_Paid__c, Amount_Due__c, Monto_Aplicado__c, Amount_Pending_to_Apply__c,
                                 (SELECT Id, Name, Procedure__r.Id, Procedure__r.Name, Procedure__r.Customer__r.Name, Procedure__r.RecordType.Name,
                                  Procedure__r.CreatedDate, Procedure__r.Consulate_Payment__c, Procedure__r.Agency_Commission__c,
                                  Procedure__r.Wholesaler_Payment__c
                                  FROM Batch_Items__r WHERE Applied_Cash__c = NULL AND Payment_Date__c = NULL)
                                 FROM Batch__c WHERE Id = :batchId];

        dcApplyPaymentController.Batch_Wrapper result;
        
        if(batchs.size() > 0) {
            Batch__c batch = batchs[0];
            result = new dcApplyPaymentController.Batch_Wrapper();
            result.batch_Id = batchId;
            result.batch_Name = batch.Name;
            result.amount_Paid = batch.Amount_Paid__c; //Monto Pagado
            result.amount_Due = batch.Amount_Due__c;  //Monto Pendiente por Pagar
            result.amount_Applied = batch.Monto_Aplicado__c; //Monto Aplicado
            result.amount_Pending_to_Apply = batch.Amount_Pending_to_Apply__c; //Monto pendiente por aplicar
            result.procedure_count_to_Apply = batch.Batch_Items__r.size(); //Cantidad de Tramites pendientes por aplicar
            result.batch_items = new List<dcApplyPaymentController.Batch_Item_Wrapper>();
            for(Batch_Item__c item: batch.Batch_Items__r) {
                dcApplyPaymentController.Batch_Item_Wrapper itemWrapper = new dcApplyPaymentController.Batch_Item_Wrapper();
                itemWrapper.batch_item_id = item.Id;
                itemWrapper.batch_item_Name = item.Name;
                itemWrapper.procedure_id = item.Procedure__r.Id;
                itemWrapper.procedure_Name = item.Procedure__r.Name;
                itemWrapper.procedureNameLink = '/'+item.Procedure__r.Id;
                itemWrapper.procedure_Contact = item.Procedure__r.Customer__r.Name;
                itemWrapper.procedure_type = item.Procedure__r.RecordType.Name;
                itemWrapper.procedure_date = item.Procedure__r.CreatedDate.format('MM/dd/yyyy HH:mm');
                itemWrapper.procedure_consulate_payment = item.Procedure__r.Consulate_Payment__c;
                itemWrapper.procedure_agency_comission = item.procedure__r.Agency_Commission__c;
                itemWrapper.procedure_distributor_payment = item.Procedure__r.Wholesaler_Payment__c;
                result.batch_items.add(itemWrapper);
            }
        }
        return result;
    }


    /**
    * @description Apply Payments to selected batch_items
    * @author Silvia Velazquez | 08-27-2020 
    * @param items 
    * @return String 
    **/
    public static String applyPayment(dcApplyPaymentController.ItemsToApply_Wrapper items){
        if(!(Schema.sObjectType.Batch__c.isAccessible() &&
             Schema.sObjectType.Batch__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Batch__c.fields.Recipient__c.isAccessible() &&
             Schema.sObjectType.Account.fields.Name.isAccessible() &&
             Schema.sObjectType.Account.fields.Id.isAccessible() &&
             Schema.sObjectType.Batch__c.fields.Amount_Pending_to_Apply__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Id.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Batch__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Name.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Applied_Cash__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Payment_Date__c.isAccessible() &&
             Schema.sObjectType.Batch_Item__c.fields.Procedure__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Consulate_Payment__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Agency_Payment_Date__c.isAccessible() &&
             Schema.sObjectType.Procedure__c.fields.Agency_Commission__c.isAccessible())) {
            DCException.throwPermiException('CustomMetadataUploadController.loadCustomMetadataMetadata');
        }
        
        List<Batch__c> batches = [SELECT Id, Recipient__r.Name, Recipient__r.Id, Amount_Pending_to_Apply__c,
                                  (SELECT Id, Name, Applied_Cash__c, Payment_Date__c, Procedure__c,
                                   Procedure__r.Consulate_Payment__c, Procedure__r.Agency_Commission__c,Procedure__r.Agency_Payment_Date__c
                                   FROM Batch_Items__r WHERE Id IN :items.itemsToApply)
                                  FROM Batch__c WHERE Id = :items.batchId];

        String result;
        System.debug('Lote: '+ items.batchId);
        System.debug('Articulos de Lote: '+ items.itemsToApply);
        
        if(batches.size() > 0) {
            Batch__c batch = batches[0];
            if(batch.Batch_Items__r.size() > 0 && batch.Amount_Pending_to_Apply__c > 0) {  //Validating if exists an amount pending to apply
                List<SObject> records = new List<SObject>(); // List to Update Batch_Items and Procedures and to Create Transactions
                //List<Transaction__c> transactions = new List<Transaction__c>(); //List to Insert new Transactions
                String agencyId = (batch.Recipient__r.Name == 'District Cuba') ? batch.Recipient__r.Id : null;

                for(Batch_Item__c item : batch.Batch_Items__r) {  //For each item, 4 elements are inserted in records list to finally make a DML statement over the list.
                    Decimal amount = (item.Procedure__r.Agency_Commission__c != null) ? item.Procedure__r.Agency_Commission__c : 0;
                    Transaction__c creditTransaction = new Transaction__c(Agency__c = agencyId,
                                                                          GL_Account__c = 'Operaciones',
                                                                          Description__c = 'Transacción automática para la aplicación de un pago.',
                                                                          Date__c = dateTime.now(),
                                                                          Lote__c = batch.Id,
                                                                          Batch_Item__c = item.Id,
                                                                          Monto__c = amount,
                                                                          Procedure__c = item.Procedure__c);
                    records.add(creditTransaction);
                    Transaction__c debitTransaction = new Transaction__c(Agency__c = agencyId,
                                                                         GL_Account__c = 'Escrow',
                                                                         Description__c = 'Transacción automática para la aplicación de un pago.',
                                                                         Date__c = dateTime.now(),
                                                                         Lote__c = batch.Id,
                                                                         Batch_Item__c = item.Id,
                                                                         Monto__c = -1 * amount);
                    records.add(debitTransaction);
                    Transaction__c debitEscrowTransaction = new Transaction__c(Agency__c = agencyId,
                                                                         GL_Account__c = 'Escrow',
                                                                         Description__c = 'Transacción automática para la aplicación de un pago.',
                                                                          Date__c = dateTime.now(),
                                                                         Lote__c = batch.Id,
                                                                         Batch_Item__c = item.Id,
                                                                         Monto__c = -1 * item.Procedure__r.Consulate_Payment__c);
                    records.add(debitEscrowTransaction);
                    Transaction__c creditEscrowTransaction = new Transaction__c(Agency__c = agencyId,
                                                                         GL_Account__c = 'Escrow',
                                                                         Description__c = 'Transacción automática para la aplicación de un pago.',
                                                                         Date__c = dateTime.now(),
                                                                         Lote__c = batch.Id,
                                                                         Batch_Item__c = item.Id,
                                                                         Monto__c = item.Procedure__r.Consulate_Payment__c,
                                                                         Procedure__c = item.Procedure__c);
                    records.add(creditEscrowTransaction);

                    item.Applied_Cash__c = amount + item.Procedure__r.Consulate_Payment__c;
                    item.Payment_Date__c = Datetime.now();
                    item.Procedure__r.Agency_Payment_Date__c = Date.today();
                    records.add(item);
                    records.add(item.Procedure__r);
                }

                records.sort(); //Sorting by Sobject Type to prevent Error: "System.TypeException: Cannot have more than 10 chunks in a single operation."
                try {
                    //upsert records;
                    CRUDEnforce.dmlUpsert(records, 'dcApplyPaymentUtility.applyPayment');
                    result = 'OK';
                } catch (DmlException e) {
                    return 'Error: A DML exception has occurred: ' + e.getMessage();
                }
            }
            else{
                result = 'Error: No hay Articulos de Lote pendientes por aplicar pagos ó El Lote no dispone de un monto pendiente por aplicar.';
            }
        }
        else{
            result = 'Error: No se encontró un Lote con el valor del Id proporcionado.';
        }

        return result;
    }
}