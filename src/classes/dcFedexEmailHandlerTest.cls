/**
 * @File Name          : dcFedexEmailHandlerTest.cls
 * @Description        : Test Class for dcFedexEmailHandler
 * @Author             : Silvia Velazquez
 * @Group              : 
 * @Last Modified By   : Ibrahim Napoles
 * @Last Modified On   : 08-26-2020
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    22/7/2020   Silvia Velazquez     Initial Version
**/
@isTest
private class dcFedexEmailHandlerTest {
    @isTest
    static void trackMailEnvioOK(){
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Map<String,String> data = TestDataFactory.generateHTMLMailMessage(true, true, 'Envio',false);
        email.subject = data.get('subject');
        email.fromname = data.get('fromName');
        env.fromAddress = data.get('fromAddress');
        email.htmlBody = data.get('htmlBody');
        Test.startTest();
        dcFedexEmailHandler emailProcess = new dcFedexEmailHandler();
        Messaging.InboundEmailResult result = emailProcess.handleInboundEmail(email, env);
        Test.stopTest();
        // Verify that a fake result is returned
        System.assertEquals(true,result.success, ''); 
        String messageText = 'The following email message has been successfully parsed';
        System.assertEquals(true,result.message.containsIgnoreCase(messageText), ''); 
    }

    @isTest
    static void trackMailEnvioWithDeliveryOK(){
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Map<String,String> data = TestDataFactory.generateHTMLMailMessage(true, true, 'Envio',true);
        email.subject = data.get('subject');
        email.fromname = data.get('fromName');
        env.fromAddress = data.get('fromAddress');
        email.htmlBody = data.get('htmlBody');
        Test.startTest();
        dcFedexEmailHandler emailProcess = new dcFedexEmailHandler();
        Messaging.InboundEmailResult result = emailProcess.handleInboundEmail(email, env);
        Test.stopTest();
        // Verify that a fake result is returned
        System.assertEquals(true,result.success, ''); 
        String messageText = 'The following email message has been successfully parsed';
        System.assertEquals(true,result.message.containsIgnoreCase(messageText), ''); 
    }

    @isTest
    static void trackMailEnvioNoValueInMail(){
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Map<String,String> data = TestDataFactory.generateHTMLMailMessage(false, false, 'Envio',false);
        email.subject = data.get('subject');
        email.fromname = data.get('fromName');
        env.fromAddress = data.get('fromAddress');
        email.htmlBody = data.get('htmlBody');
        Test.startTest();
        dcFedexEmailHandler emailProcess = new dcFedexEmailHandler();
        Messaging.InboundEmailResult result = emailProcess.handleInboundEmail(email, env);
        Test.stopTest();
        // Verify that a fake result is returned
        System.assertEquals(true,!result.success, ''); 
        String messageText = 'No "Tracking Number xxxxxxxxxxxx" pattern was encountered in this email body';
        System.assertEquals(true,result.message.containsIgnoreCase(messageText), ''); 
    }

    @isTest
    static void trackMailEnvioNoValueInSF(){
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Map<String,String> data = TestDataFactory.generateHTMLMailMessage(true, false, 'Envio',false);
        email.subject = data.get('subject');
        email.fromname = data.get('fromName');
        env.fromAddress = data.get('fromAddress');
        email.htmlBody = data.get('htmlBody');
        Test.startTest();
        dcFedexEmailHandler emailProcess = new dcFedexEmailHandler();
        Messaging.InboundEmailResult result = emailProcess.handleInboundEmail(email, env);
        Test.stopTest();
        // Verify that a fake result is returned
        System.assertEquals(true,!result.success, ''); 
        String messageText = 'is not in Salesforce';
        System.assertEquals(true,result.message.containsIgnoreCase(messageText), ''); 
    }

    @isTest
    static void trackMailTraceoOK(){
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Map<String,String> data = TestDataFactory.generateHTMLMailMessage(true, true, 'Traceo',false);
        email.subject = data.get('subject');
        email.fromname = data.get('fromName');
        env.fromAddress = data.get('fromAddress');
        email.htmlBody = data.get('htmlBody');
        Test.startTest();
        dcFedexEmailHandler emailProcess = new dcFedexEmailHandler();
        Messaging.InboundEmailResult result = emailProcess.handleInboundEmail(email, env);
        Test.stopTest();
        // Verify that a fake result is returned
        System.assertEquals(true,result.success, ''); 
        String messageText = 'The following email message has been successfully parsed';
        System.assertEquals(true,result.message.containsIgnoreCase(messageText), ''); 
    }

    @isTest
    static void trackMailTraceoNoValueInMail(){
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Map<String,String> data = TestDataFactory.generateHTMLMailMessage(false, false, 'Traceo',false);
        email.subject = data.get('subject');
        email.fromname = data.get('fromName');
        env.fromAddress = data.get('fromAddress');
        email.htmlBody = data.get('htmlBody');
        Test.startTest();
        dcFedexEmailHandler emailProcess = new dcFedexEmailHandler();
        Messaging.InboundEmailResult result = emailProcess.handleInboundEmail(email, env);
        Test.stopTest();
        // Verify that a fake result is returned
        System.assertEquals(true,!result.success, ''); 
        String messageText = 'No "Tracking Number xxxxxxxxxxxx" pattern was encountered in this email body';
        System.assertEquals(true,result.message.containsIgnoreCase(messageText), ''); 
    }

    @isTest
    static void trackMailTraceoNoValueInSF(){
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Map<String,String> data = TestDataFactory.generateHTMLMailMessage(true, false, 'Traceo',false);
        email.subject = data.get('subject');
        email.fromname = data.get('fromName');
        env.fromAddress = data.get('fromAddress');
        email.htmlBody = data.get('htmlBody');
        Test.startTest();
        dcFedexEmailHandler emailProcess = new dcFedexEmailHandler();
        Messaging.InboundEmailResult result = emailProcess.handleInboundEmail(email, env);
        Test.stopTest();
        // Verify that a fake result is returned
        System.assertEquals(true,!result.success, ''); 
        String messageText = 'is not in Salesforce';
        System.assertEquals(true,result.message.containsIgnoreCase(messageText), ''); 
    }

}