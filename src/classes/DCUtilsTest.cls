/**
 * @description       : 
 * @author            : 
 * @group             : 
 * @last modified on  : 08-24-2020
 * @last modified by  : Ibrahim Napoles
 * Modifications Log 
 * Ver   Date         Author            Modification
 * 1.0   08-24-2020   Ibrahim Napoles   Initial Version
**/
@IsTest
private class DCUtilsTest {
    @IsTest
    static void fetchRecordTypeValues(){
        Test.startTest();
        System.assertNotEquals(true, DCUtils.fetchRecordTypeValues('Contact').isEmpty(), 'Check Record Types list');
        Test.stopTest();
    }

    @IsTest
    static void getRecTypeId(){
        Test.startTest();
        System.assertNotEquals(null , DCUtils.getRecTypeId('Contact', DCUtils.fetchRecordTypeValues('Contact')[0]), 'Check Rec Type');
        Test.stopTest();
    }

    @IsTest
    static void getRecTypeIdByDevName(){
        Test.startTest();
        System.assertNotEquals(null, DCUtils.getRecTypeIdByDevName('Contact', 'Cliente'), 'Check Record Type by developer name');
        Test.stopTest();
    }
}