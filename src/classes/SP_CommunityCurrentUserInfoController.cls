/**
 * @File Name          : SP_CommunityCurrentUserInfoController.cls
 * @Description        : 
 * @Author             : Ibrahim Napoles
 * @Group              : 
 * @Last Modified By   : Ibrahim Napoles
 * @Last Modified On   : 12/13/2019, 9:44:17 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    12/13/2019   Ibrahim Napoles     Initial Version
**/

public class SP_CommunityCurrentUserInfoController {

	@AuraEnabled
	public static string getCommunityUserCustomName() {
		return getCustomName(UserInfo.getFirstName(), UserInfo.getLastName());
	}

	public static string getCustomName(string firstName, string lastName) {
		string result = null;
		if (String.isNotBlank(firstName)) {
			result = firstName;
		}
		if (String.isNotBlank(lastName)) {
			if (result != null) {
				result += (' ' + lastName.substring(0, 1) + '.');
			} else {
				result = lastName;
			}
		}
		if (result != null)
			return result;
		//else...
		return '';
	}
}