/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 10-19-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   10-19-2020   Elizabeth Betancourt Herrera   Initial Version
**/
public with sharing class dcSearchProcedureToBatch {
    
        @AuraEnabled
        public String agency {get; set;}
        @AuraEnabled
        public String status {get; set;}
        @AuraEnabled
        public Boolean noBatch {get; set;}
        @AuraEnabled
        public Boolean noDistributor {get; set;}
        @AuraEnabled
        public String filter {get; set;}

        @AuraEnabled
        public Integer pageSize {get; set;}
        @AuraEnabled
        public Integer offset {get; set;}
        @AuraEnabled
        public String sortBy {get; set;}
        @AuraEnabled
        public Boolean isDescending {get; set;}
        
        public dcSearchProcedureToBatch() {
            agency = '';
            status = '';
            noBatch = true;
            noDistributor = true;
            filter = '';
            pageSize = 10;
            offset = 0;
            sortBy = 'Name, CreatedDate';
            isDescending = false;
        }
    
        public dcSearchProcedureToBatch(dcSearchProcedureToBatch searchProcedure){
            agency = searchProcedure.agency;
            status = searchProcedure.status;
            noBatch = searchProcedure.noBatch;
            noDistributor = searchProcedure.noDistributor;
            filter = searchProcedure.filter;
            pageSize = searchProcedure.pageSize;
            offset = searchProcedure.offset;
            sortBy = searchProcedure.sortBy;
            isDescending = searchProcedure.isDescending;
        }
}