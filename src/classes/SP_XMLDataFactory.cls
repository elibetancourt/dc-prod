/**
 * @description       :
 * @author            : Silvia Velazquez
 * @group             :
 * @last modified on  : 09-08-2020
 * @last modified by  : Silvia Velazquez
 * Modifications Log
 * Ver   Date         Author                   Modification
 * 1.0   07-22-2020   William Santana Méndez   Initial Version
 **/
@isTest
public class SP_XMLDataFactory {

    public static Contact createContact(){
        Contact c = new Contact(FirstName='Luis',LastName = 'Suarez', Second_Last_Name__c='Muñoz', MailingCity='Miami' , Work_City__c='Miami Lakes');

        return c;
    }

    public static Account createAccount(){
     Account acc = new Account(Active__c = 'Yes',Name = 'District Cuba',Phone='2023211169',Type='Mayorista',Sigla__c = 'DCW');

     return acc;
    }



    public static List<Manifest__c> createManifestList(){
        List<Manifest__c> manifestList=new List<Manifest__c>();
        //solo el campo Tipo es obligatorio
        Manifest__c manifest1=new Manifest__c(Type__c='Pasaporte 1ra Vez');
        Manifest__c manifest2=new Manifest__c(Type__c='Prorroga');
        Manifest__c manifest3=new Manifest__c(Type__c='Renovacion de Pasaporte');
        Manifest__c manifest4=new Manifest__c(Type__c='Visa HE-11');
        Manifest__c manifest5=new Manifest__c(Type__c='Prorroga de Estancia');
        manifestList.add(manifest1);
        manifestList.add(manifest2);
        manifestList.add(manifest3);
        manifestList.add(manifest4);      
        manifestList.add(manifest5);
        
        return manifestList;
    }

    public static List<Procedure__c> createProcedureListFull(Contact contact1, Account account1, List<Manifest__c> manifestList){
        Date myDate = date.newInstance(2012,05,22);
        String mYear = String.valueOf(myDate.year());
        String yearTo = String.valueOf(myDate.year() + 3);

        List<Procedure__c> procedureList=new List<Procedure__c>();

        // Id p1vRT = SP_RecordType_Controller.getRecTypeIdByDevName('Procedure__c', 'Pasaporte_1ra_Vez');

        Id rType1 = Schema.SObjectType.Procedure__c.getRecordTypeInfosByName().get('Pasaporte 1ra Vez').getRecordTypeId(); //getting the record type Id       
        //generate procedure1 with rtype: Pasaporte 1ra Vez
        Procedure__c procedure1=new Procedure__c(
            RecordTypeId = rType1,
            Customer__c=contact1.Id,
            Agency__c=account1.Id,
            First_Name__c='Pepe 2',
            Middle_Name__c='Montes',
            Last_Name__c='Perez',
            Second_Last_Name__c='Muñoz',
            Father_Name__c='Pepe 1',
            Mother_Name__c='Maria',
            Hair_Color__c='Negro',
            Eyes_Color__c='Negros',
            Skin_Color__c='Blanca',
            Height__c=180,
            Gender__c='Masculino',
            Birth_Country__c='Cuba',
            Birth_Province__c='La Habana',
            Departure_Date__c=myDate,
            Birthday__c=myDate,
            Category__c='Permanente',
            Street__c='Calle 1',
            Residence_Country__c='US',
            State__c='AZ',
            City__c='Ciudad 1',
            Postal_Code__c='10400',
            Phone__c='555555',
            Email__c='j1@example.org',
            Work_Name__c='Trabajo 1',
            Work_State__c='AZ',
            Title__c='Ocupacion 1',
            Nivel_de_Escolaridad__c='Primario',
            Reference_Full_Name__c='Pepe 1',
            Reference_Address__c='Calle 1',
            Residence_Address_1__c='Calle 1',
            Residence_Year_1_From__c= mYear,
            Residence_Year_1_To__c=yearTo,
            Birth_Certificate_Issue_Date__c=myDate,
            Birth_Certificate_Issue_At__c='SANTIAGO DE CUBA',
            Manifest__c=manifestList[0].Id);

        Id rType2 = Schema.SObjectType.Procedure__c.getRecordTypeInfosByName().get('Prorroga').getRecordTypeId(); //getting the record type Id
        //generate procedure2 with rtype: Prorroga
        Procedure__c procedure2=new Procedure__c(
            RecordTypeId=rType2,
            Customer__c=contact1.Id,
            Agency__c=account1.Id,
            First_Name__c='Coco 2',
            Middle_Name__c='Montes',
            Last_Name__c='Sosa',
            Second_Last_Name__c='Muñoz',
            Father_Name__c='Chicho 1',
            Mother_Name__c='Rosa',
            Hair_Color__c='Rubio',
            Eyes_Color__c='Negros',
            Skin_Color__c='Blanca',
            Height__c=160,
            Gender__c='Masculino',
            Birth_Country__c='Cuba',
            Birth_Province__c='La Habana',
            Departure_Date__c=myDate,
            Birthday__c=myDate,
            Category__c='Permanente',
            Street__c='Calle 1',
            Residence_Country__c='US',
            State__c='AZ',
            City__c='Ciudad 1',
            Postal_Code__c='10400',
            Phone__c='555555',
            Email__c='j1@example.org',
            Work_Name__c='Trabajo 1',
            Work_State__c='AZ',
            Title__c='Ocupacion 1',
            Nivel_de_Escolaridad__c='Primario',
            Reference_Full_Name__c='Pepe 1',
            Reference_Address__c='Calle 1',
            Reference_Provincia__c='La Habana',           
            Residence_Address_1__c='Calle 1',
            Residence_Year_1_From__c=mYear,
            Residence_Year_1_To__c=yearTo,
            Birth_Certificate_Issue_Date__c=myDate,
            Birth_Certificate_Issue_At__c='SANTIAGO DE CUBA',            
            Manifest__c=manifestList[1].Id);

        Id rType3 = Schema.SObjectType.Procedure__c.getRecordTypeInfosByName().get('Renovacion de Pasaporte').getRecordTypeId(); //getting the record type Id   
        //generate procedure3 with rtype: Renovacion de Pasaporte
        Procedure__c procedure3=new Procedure__c(
            RecordTypeId=rType3,
            Customer__c=contact1.Id,
            Agency__c=account1.Id,
            First_Name__c='Papo 2',
            Middle_Name__c='Montes',
            Last_Name__c='Tun',
            Second_Last_Name__c='Muñoz',
            Father_Name__c='Siso 1',
            Mother_Name__c='Sonia',
            Hair_Color__c='Negro',
            Eyes_Color__c='Negros',
            Skin_Color__c='Blanca',
            Height__c=190,
            Gender__c='Masculino',
            Birth_Country__c='Cuba',
            Birth_Province__c='La Habana',
            Departure_Date__c=myDate,
            Birthday__c=myDate,
            Category__c='Permanente',
            Street__c='Calle 1',
            Residence_Country__c='US',
            State__c='AZ',
            City__c='Ciudad 1',
            Postal_Code__c='10400',
            Phone__c='555555',
            Email__c='j1@example.org',
            Work_Name__c='Trabajo 1',
            Work_State__c='AZ',
            Title__c='Ocupacion 1',
            Nivel_de_Escolaridad__c='Primario',
            Reference_Full_Name__c='Pepe 1',
            Reference_Address__c='Calle 1',
            Reference_Provincia__c='La Habana',
            Residence_Address_1__c='Calle 1',
            Residence_Year_1_From__c=mYear,
            Residence_Year_1_To__c=yearTo,
            Birth_Certificate_Issue_Date__c=myDate,
            Birth_Certificate_Issue_At__c='SANTIAGO DE CUBA',
            Manifest__c=manifestList[2].Id);

        Id rType4 = Schema.SObjectType.Procedure__c.getRecordTypeInfosByName().get('Visa HE-11').getRecordTypeId(); //getting the record type Id
        //generate procedure4 with rtype: Visa HE-11
        Procedure__c procedure4=new Procedure__c(
            RecordTypeId=rType4,
            Customer__c=contact1.Id,
            Agency__c=account1.Id,
            First_Name__c='Tito 2',
            Middle_Name__c='Montes',
            Last_Name__c='Saen',
            Second_Last_Name__c='Garcia',
            Father_Name__c='Sume 1',
            Mother_Name__c='Fola',
            Hair_Color__c='Negro',
            Eyes_Color__c='Negros',
            Skin_Color__c='Blanca',
            Height__c=160,
            Gender__c='Masculino',
            Birth_Country__c='Cuba',
            Birth_Province__c='La Habana',
            Departure_Date__c=myDate,
            Birthday__c=myDate,
            Category__c='Permanente',
            Street__c='Calle 1',
            Residence_Country__c='US',
            State__c='AZ',
            City__c='Ciudad 1',
            Postal_Code__c='10400',
            Phone__c='555555',
            Email__c='j1@example.org',
            Work_Name__c='Trabajo 1',
            Work_State__c='AZ',
            Title__c='Ocupacion 1',
            Nivel_de_Escolaridad__c='Primario',
            Reference_Full_Name__c='Muñoz',
            Reference_Address__c='Calle 1',
            Reference_Provincia__c='La Habana',
            Residence_Address_1__c='Calle 1',
            Residence_Year_1_From__c=mYear,
            Residence_Year_1_To__c=yearTo,
            Birth_Certificate_Issue_Date__c=myDate,
            Birth_Certificate_Issue_At__c='SANTIAGO DE CUBA',
            Manifest__c=manifestList[3].Id);

            Id rType5 = Schema.SObjectType.Procedure__c.getRecordTypeInfosByName().get('Prorroga de Estancia').getRecordTypeId(); //getting the record type Id
        //generate procedure2 with rtype: Prorroga
        Procedure__c procedure5=new Procedure__c(
            RecordTypeId=rType5,
            Customer__c=contact1.Id,
            Agency__c=account1.Id,
            First_Name__c='Coco 6',
            Middle_Name__c='Montess',
            Last_Name__c='Sosas',
            Second_Last_Name__c='Muñoz',
            Father_Name__c='Chicho 12',
            Mother_Name__c='Rosario',
            Hair_Color__c='Rubio',
            Eyes_Color__c='Negros',
            Skin_Color__c='Blanca',
            Height__c=160,
            Gender__c='Masculino',
            Birth_Country__c='Cuba',
            Birth_Province__c='La Habana',
            Departure_Date__c=myDate,
            Birthday__c=myDate,
            Category__c='Permanente',
            Street__c='Calle 10',
            Residence_Country__c='US',
            State__c='AZ',
            City__c='Ciudad 1',
            Postal_Code__c='10400',
            Phone__c='555555',
            Email__c='j2@example.org',
            Work_Name__c='Trabajo 1',
            Work_State__c='AZ',
            Title__c='Ocupacion 1',
            Nivel_de_Escolaridad__c='Primario',
            Reference_Full_Name__c='Pepe 10',
            Reference_Address__c='Calle 10',
            Reference_Provincia__c='La Habana',           
            Residence_Address_1__c='Calle 11',
            Residence_Year_1_From__c=mYear,
            Residence_Year_1_To__c=yearTo,
            Birth_Certificate_Issue_Date__c=myDate,
            Birth_Certificate_Issue_At__c='SANTIAGO DE CUBA',            
            Manifest__c=manifestList[4].Id);
                    
        procedureList.add(procedure1);
        procedureList.add(procedure2);
        procedureList.add(procedure3);
        procedureList.add(procedure4);
        procedureList.add(procedure5);

        return procedureList;
    }
}