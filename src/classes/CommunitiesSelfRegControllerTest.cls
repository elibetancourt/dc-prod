/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
@IsTest 
private class CommunitiesSelfRegControllerTest {
    @IsTest
    public static void testCommunitiesSelfRegController() {
        CommunitiesSelfRegController controller = new CommunitiesSelfRegController();
        controller.firstName = 'FirstName';
        controller.lastName = 'LastName';
        controller.email = 'test@force.com';
        controller.communityNickname = 'test';
        
        // registerUser will always return null when the page isn't accessed as a guest user
        System.assertEquals(null ,controller.registerUser(), 'RegisterUser will always return null when the page isnt accessed as a guest user');    
        
        controller.password = 'abcd1234';
        controller.confirmPassword = 'abcd123';
        System.assertEquals(null , controller.registerUser(), '');  
    }    
}