<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BROOMFIELD</label>
    <protected>false</protected>
    <values>
        <field>City__c</field>
        <value xsi:type="xsd:string">BROOMFIELD</value>
    </values>
    <values>
        <field>Code__c</field>
        <value xsi:type="xsd:string">6228</value>
    </values>
    <values>
        <field>State__c</field>
        <value xsi:type="xsd:string">CO</value>
    </values>
</CustomMetadata>
