/**
 * @description       : 
 * @author            : Elizabeth Betancourt Herrera
 * @group             : 
 * @last modified on  : 09-23-2020
 * @last modified by  : Elizabeth Betancourt Herrera
 * Modifications Log 
 * Ver   Date         Author                         Modification
 * 1.0   09-18-2020   Elizabeth Betancourt Herrera   Initial Version
**/
trigger dcEmployeeTrigger on User_Agency_RelationShip__c (after insert, after update, after delete) {
                                                            
    new dcEmployeeTriggerHandler().updateCountAgencyUserEmployee();   
}