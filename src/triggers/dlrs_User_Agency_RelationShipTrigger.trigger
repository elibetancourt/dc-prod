/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
trigger dlrs_User_Agency_RelationShipTrigger on User_Agency_RelationShip__c
    (before delete, before insert, before update, after delete, after insert, after undelete, after update)
{
    dlrs.RollupService.triggerHandler(User_Agency_RelationShip__c.SObjectType);
}